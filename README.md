# TeneJob Workers API

<img src="https://img.shields.io/badge/version-1.0.0--RELEASE-green.svg">
&nbsp;
<img src="https://img.shields.io/badge/spring--boot-1.5.16-green.svg">
&nbsp;
<img src="https://img.shields.io/badge/maven-3.3.9-green.svg">
&nbsp;
<img src="https://img.shields.io/badge/test--coverage-62%25-orange.svg">
&nbsp;

This API was developer to TeneJob company to match workers and shifts in order to organize them in the most optimal way. Running the API will be accessible on the url http://localhost:2030 .

(This API is cofigured to connect with a PostrgeSQL database on Docker http://192.168.99.100:5432 with user and password specified on the **application.yml**.)

You can run it from your IDE (Eclipse, Spring Tools Suite), or with Maven from command line with:
```
$ mvn spring-boot:run
```


## Endpoints

### Workers
| Method | endpoint                         | Description                                                          |
| ------ | -------------------------------- | -------------------------------------------------------------------- |
| GET    | **/workers**                     | Return all workers stored in database.                               |
| POST   | **/workers**                     | Save a new worker into the database.                                 |
| GET    | **/workers/{id}**                | Return the worker with the specific _id_ from the database.          |
| PUT    | **/workers/{id}**                | Will update the worker with the specific _id_.                       |
| DELETE | **/workers/{id}**                | Will delete the worker with the specific _id_.                       |
| GET    | **/workers/{id}/availabilities** | Will return all availabilities from the worker specified by the _id_ |

### Shifts
| Method | endpoint         | Description                                                |
| ------ | ---------------- | ---------------------------------------------------------- |
| GET    | **/shifts**      | Return all shifts stored in database.                      |
| POST   | **/shifts**:     | Save a new shift into the database.                        |
| GET    | **/shifts/{id}** | Return the shift with the specific _id_ from the database. |
| PUT    | **/shifts/{id}** | Will update the shift with the specific _id_.              |
| DELETE | **/shifts/{id}** | Will delete the shift with the specific _id_.              |

### Matchings
| Method | endpoint       | Description                                                                                |
| ------ | -------------- | ------------------------------------------------------------------------------------------ |
| GET    | **/matchings** | Return the optimal matchings between workers and shifts stored in the database.            |
| POST   | **/mappings**  | Return the optimal matchings between workers and shifts sended by request to the endpoint. |

## Endpoints documentation

Running the API on will be accessible in the url http://localhost:2030, and also all documentated endpoints on the next url: http://localhost:2030/swagger-ui.html

## Code test coverage

You can check test coverage running this simple commands.

```
$ mvn clean test
$ mvn jacoco:report
```

You could check it on **/target/site/jacoco/index.html** in the web browser.