package com.tenejob.tenejobworkersapi.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tenejob.tenejobworkersapi.models.Worker;
import com.tenejob.tenejobworkersapi.repository.WorkerRepository;

@Service
public class WorkerService {

	@Autowired
	private WorkerRepository workerRepository;

	public List<Worker> findAll() {
		return this.workerRepository.findAll();
	}

	public Worker save(Worker worker) {
		return this.workerRepository.saveAndFlush(worker);
	}

	public Worker findOne(Integer id) {
		return this.workerRepository.findOne(id);
	}

	public void delete(Integer id) {
		this.workerRepository.delete(id);
	}
}
