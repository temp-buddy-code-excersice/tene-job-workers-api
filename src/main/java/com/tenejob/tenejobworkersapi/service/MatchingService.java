package com.tenejob.tenejobworkersapi.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//import com.tenejob.tenejobworkersapi.repository.MatchingRepository;
import com.tenejob.tenejobworkersapi.repository.ShiftRepository;
import com.tenejob.tenejobworkersapi.repository.WorkerRepository;
import com.tenejob.tenejobworkersapi.utils.InputLists;

@Service()
public class MatchingService {

	// @Autowired
	// private MatchingRepository matchingRepository;

	@Autowired
	private WorkerRepository workerRepository;

	@Autowired
	private ShiftRepository shiftRepository;

	// public List<Matching> findAll() {
	// return this.matchingRepository.findAll();
	// }

	public InputLists getAllWorkersAndShifts() {
		return new InputLists(this.workerRepository.findAll(), this.shiftRepository.findAll());
	}

}
