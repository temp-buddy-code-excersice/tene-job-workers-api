package com.tenejob.tenejobworkersapi.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tenejob.tenejobworkersapi.models.Shift;
import com.tenejob.tenejobworkersapi.repository.ShiftRepository;

@Service
public class ShiftService {

	@Autowired
	private ShiftRepository shiftRepository;

	public List<Shift> findAll() {
		return this.shiftRepository.findAll();
	}

	public Shift save(Shift shift) {
		return this.shiftRepository.save(shift);
	}

	public Shift findOne(Integer id) {
		return this.shiftRepository.findOne(id);
	}

	public void delete(Integer id) {
		this.shiftRepository.delete(id);
	}
}
