package com.tenejob.tenejobworkersapi.utils;


import java.util.ArrayList;
import java.util.List;

import com.tenejob.tenejobworkersapi.models.Matching;

//@Data
public class OutputMappings {

	private List<Matching> mappings;

	private Double totalCost;

	public OutputMappings() {
		this.mappings = new ArrayList<>();
		this.totalCost = 0.00;
	}

	public void addTotalCost(Double payrate) {
		this.totalCost += payrate;
	}

	public List<Matching> getMappings() {
		return this.mappings;
	}

	public Double getTotalCost() {
		return this.totalCost;
	}

	public void setMappings(List<Matching> mappings) {
		this.mappings = mappings;
	}

	public void setTotalCost(Double totalCost) {
		this.totalCost = totalCost;
	}

}
