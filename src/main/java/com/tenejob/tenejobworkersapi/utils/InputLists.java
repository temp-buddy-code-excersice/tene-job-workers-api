package com.tenejob.tenejobworkersapi.utils;


import java.util.ArrayList;
import java.util.List;

import com.tenejob.tenejobworkersapi.models.Shift;
import com.tenejob.tenejobworkersapi.models.Worker;

//@Data
public class InputLists {

	private List<Worker> workers;

	private List<Shift> shifts;

	public InputLists() {
		this.workers = new ArrayList<>();
		this.shifts = new ArrayList<>();
	}

	public InputLists(List<Worker> workers, List<Shift> shifts) {
		this.workers = workers;
		this.shifts = shifts;
	}

	public List<Worker> getWorkers() {
		return workers;
	}

	public void setWorkers(List<Worker> workers) {
		this.workers = workers;
	}

	public List<Shift> getShifts() {
		return shifts;
	}

	public void setShifts(List<Shift> shifts) {
		this.shifts = shifts;
	}

}
