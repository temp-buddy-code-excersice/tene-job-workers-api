package com.tenejob.tenejobworkersapi.utils;


import java.util.ArrayList;
import java.util.List;

import com.tenejob.tenejobworkersapi.models.Shift;
import com.tenejob.tenejobworkersapi.models.Worker;

//@Data
public class MatchingPair {

	private Shift shift;

	private List<Worker> workers;

	public MatchingPair() {
		this.shift = new Shift();
		this.workers = new ArrayList<>();
	}

	public MatchingPair(Shift shift, List<Worker> workers) {
		this.shift = shift;
		this.workers = workers;
	}

	public Shift getShift() {
		return shift;
	}

	public void setShift(Shift shift) {
		this.shift = shift;
	}

	public List<Worker> getWorkers() {
		return workers;
	}

	public void setWorkers(List<Worker> workers) {
		this.workers = workers;
	}

}
