package com.tenejob.tenejobworkersapi.controller;


import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tenejob.tenejobworkersapi.models.Shift;
import com.tenejob.tenejobworkersapi.service.ShiftService;

import io.swagger.annotations.Api;

@RestController
@Api(value = "shiftController", description = "CRUD endpoints for shifts")
@RequestMapping(value = "/shifts")
public class ShiftController {

	@Autowired
	private ShiftService shiftService;

	@GetMapping
	public List<Shift> findShifts() {
		return this.shiftService.findAll().stream().sorted(Comparator.comparingInt(Shift::getId))
		        .collect(Collectors.toList());
	}

	@GetMapping(value = "/{id}")
	public Shift findShift(@PathVariable("id") Integer id) {
		return this.shiftService.findOne(id);
	}

	@PostMapping
	public Shift saveShift(@RequestBody Shift shift) {
		shift.setId(null);
		return this.shiftService.save(shift);
	}

	@PutMapping(value = "/{id}")
	public Shift editShift(@RequestBody Shift shift, @PathVariable Integer id) {
		shift.setId(id);
		return this.shiftService.save(shift);
	}

	@DeleteMapping(value = "/{id}")
	public void deleteShift(@PathVariable("id") Integer id) {
		this.shiftService.delete(id);
	}
}
