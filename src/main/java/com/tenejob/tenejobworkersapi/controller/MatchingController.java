package com.tenejob.tenejobworkersapi.controller;


import java.text.DecimalFormat;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.tenejob.tenejobworkersapi.models.Matching;
import com.tenejob.tenejobworkersapi.models.Shift;
import com.tenejob.tenejobworkersapi.models.Worker;
import com.tenejob.tenejobworkersapi.service.MatchingService;
import com.tenejob.tenejobworkersapi.utils.InputLists;
import com.tenejob.tenejobworkersapi.utils.MatchingPair;
import com.tenejob.tenejobworkersapi.utils.OutputMappings;

import io.swagger.annotations.Api;

@RestController()
@Api(value = "matchingController", description = "CRUD endpoints for matchings")
public class MatchingController {

	@Autowired
	private MatchingService matchingService;

	@GetMapping(value = "/matchings")
	public OutputMappings matchAllWorkersAndShifts() {
		return this.optimalMatchings(this.matchingService.getAllWorkersAndShifts());
	}

	@PostMapping(value = "/mappings")
	public OutputMappings optimalMatchings(@RequestBody InputLists inputLists) {

		OutputMappings output = new OutputMappings();
		List<Worker> workers = inputLists.getWorkers().stream()
		        .sorted(Comparator.comparingInt(w -> w.getAvailability().size())).collect(Collectors.toList());
		List<Shift> shifts = inputLists.getShifts();

		List<MatchingPair> sortedMatchingPairs = filterWOrkers(shifts, workers);

		while (!sortedMatchingPairs.isEmpty()) {
			MatchingPair matchingPair = filterWOrkers(shifts, workers).get(0);

			Shift auxShift = matchingPair.getShift();
			Worker auxWorker = matchingPair.getWorkers().stream().sorted(Comparator.comparingDouble(Worker::getPayrate))
			        .collect(Collectors.toList()).get(0);

			output.getMappings().add(new Matching(auxWorker, auxShift));
			output.addTotalCost(auxWorker.getPayrate());
			workers = workers.stream().filter(w -> w.getId() != auxWorker.getId()).collect(Collectors.toList());
			shifts = shifts.stream().filter(s -> s.getId() != auxShift.getId()).collect(Collectors.toList());
			sortedMatchingPairs = filterWOrkers(shifts, workers);
		}
		output.setTotalCost(
		        Double.parseDouble(new DecimalFormat(".##").format(output.getTotalCost()).replace(',', '.')));
		return output;
	}

	private List<MatchingPair> filterWOrkers(List<Shift> shifts, List<Worker> workers) {
		Set<MatchingPair> config = new HashSet<>();
		for (Shift shift : shifts) {
			List<Worker> aux = workers.stream()
			        .filter(w -> w.getAvailability().contains(shift.getDay().iterator().next()))
			        .collect(Collectors.toList());
			for (int i = 0; i < aux.size(); i++) {
				config.add(new MatchingPair(shift, aux));
				shifts = shifts.stream().filter(s -> s.getId() != shift.getId()).collect(Collectors.toList());
			}
		}
		return config.stream().sorted(Comparator.comparing(mPair -> mPair.getWorkers().size()))
		        .collect(Collectors.toList());
	}

}
