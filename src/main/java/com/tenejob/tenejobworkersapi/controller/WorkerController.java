package com.tenejob.tenejobworkersapi.controller;


import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tenejob.tenejobworkersapi.models.Worker;
import com.tenejob.tenejobworkersapi.service.WorkerService;
import com.tenejob.tenejobworkersapi.utils.Day;

import io.swagger.annotations.Api;

@RestController
@Api(value = "workController", description = "CRUD endpoints for workers")
@RequestMapping(value = "/workers")
public class WorkerController {

	@Autowired
	private WorkerService workerService;

	@GetMapping
	@ResponseBody
	public List<Worker> findWorkers() {
		return this.workerService.findAll().stream().sorted(Comparator.comparingInt(Worker::getId))
		        .collect(Collectors.toList());
	}

	@GetMapping(value = "/{id}")
	public Worker findWorker(@PathVariable("id") Integer id) {
		return this.workerService.findOne(id);
	}

	@GetMapping(value = "/{id}/availabilities")
	public Set<Day> findWorkerAvailabilities(@PathVariable("id") Integer id) {
		Worker w = this.workerService.findOne(id);
		if (w != null) {
			return w.getAvailability();
		} else {
			return new HashSet<>();
		}
	}

	@PostMapping
	public Worker saveWorker(@RequestBody Worker worker) {
		worker.setId(null);
		return this.workerService.save(worker);
	}

	@PutMapping(value = "/{id}")
	public Worker editWorker(@RequestBody Worker worker, @PathVariable Integer id) {
		worker.setId(id);
		return this.workerService.save(worker);
	}

	@DeleteMapping(value = "/{id}")
	public void deleteWorker(@PathVariable("id") Integer id) {
		this.workerService.delete(id);
	}

}
