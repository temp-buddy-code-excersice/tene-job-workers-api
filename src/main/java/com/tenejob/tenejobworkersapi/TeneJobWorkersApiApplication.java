package com.tenejob.tenejobworkersapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeneJobWorkersApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TeneJobWorkersApiApplication.class, args);
	}
}
