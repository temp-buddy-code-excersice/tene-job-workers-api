package com.tenejob.tenejobworkersapi.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.tenejob.tenejobworkersapi.models.Shift;

public interface ShiftRepository extends JpaRepository<Shift, Integer> {

}
