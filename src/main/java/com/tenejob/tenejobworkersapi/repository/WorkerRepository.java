package com.tenejob.tenejobworkersapi.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.tenejob.tenejobworkersapi.models.Worker;

public interface WorkerRepository extends JpaRepository<Worker, Integer> {

}
