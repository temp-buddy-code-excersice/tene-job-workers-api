package com.tenejob.tenejobworkersapi.models;


import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;

//@Entity
//@Data
public class Matching {

	@Id
	private Integer id;

	// @OneToOne(cascade = CascadeType.REMOVE, orphanRemoval = true)
	// @NotNull
	private Shift shift;

	// @OneToOne(cascade = CascadeType.REMOVE, orphanRemoval = true)
	// @NotNull
	private Worker worker;

	public Matching() {
		this.id = 0;
		this.shift = new Shift();
		this.worker = new Worker();
	}

	public Matching(Worker worker, Shift shift) {
		this.worker = worker;
		this.shift = shift;
	}

	public Integer getShiftId() {
		return this.shift.getId();
	}

	public Integer getWorkerId() {
		return this.worker.getId();
	}

	@JsonIgnore
	public Integer getId() {
		return this.id;
	}

	@JsonIgnore
	public Worker getWorker() {
		return this.worker;
	}

	@JsonIgnore
	public Shift getShift() {
		return this.shift;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setShift(Shift shift) {
		this.shift = shift;
	}

	public void setWorker(Worker worker) {
		this.worker = worker;
	}

}
