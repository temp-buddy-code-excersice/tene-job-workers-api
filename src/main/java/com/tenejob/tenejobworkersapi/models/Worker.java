package com.tenejob.tenejobworkersapi.models;


import java.util.HashSet;
import java.util.Set;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.tenejob.tenejobworkersapi.utils.Day;

@Entity
// @Data
public class Worker {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ElementCollection(fetch = FetchType.EAGER)
	private Set<Day> availability;

	private Double payrate;

	public Set<Day> getAvailability() {
		return this.availability;
	}

	public Worker() {
		this.id = 0;
		this.availability = new HashSet<>();
		this.payrate = 0.00;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getPayrate() {
		return payrate;
	}

	public void setPayrate(Double payrate) {
		this.payrate = payrate;
	}

	public void setAvailability(Set<Day> availability) {
		this.availability = availability;
	}

}
