package com.tenejob.tenejobworkersapi.models;


import java.util.HashSet;
import java.util.Set;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import com.tenejob.tenejobworkersapi.utils.Day;

@Entity
// @Data
public class Shift {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ElementCollection(fetch = FetchType.EAGER)
	@NotNull
	private Set<Day> day;

	public Shift() {
		this.id = 0;
		this.day = new HashSet<>();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Set<Day> getDay() {
		return day;
	}

	public void setDay(Set<Day> day) {
		this.day = day;
	}

}
