package com.tenejob.tenejobworkersapi.config;


import static springfox.documentation.builders.PathSelectors.regex;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig extends WebMvcConfigurationSupport {

	@Bean
	public Docket productApi() {
		return new Docket(DocumentationType.SWAGGER_2).select()
		        .apis(RequestHandlerSelectors.basePackage("com.tenejob.tenejobworkersapi.controller"))
		        .paths(regex("/.*")).build().apiInfo(metaData());
	}

	private ApiInfo metaData() {
		return new ApiInfoBuilder().title("Tene-Job Workers API")
		        .description("\"Spring Boot REST API for organize workers and shifts fot TeneJob company.\"")
		        .version("1.0.0").license("MIT License")
		        .licenseUrl("https://gitlab.com/temp-buddy-code-excersice/tene-job-workers-api/blob/develop/LICENSE")
		        .contact(new Contact("Gabriel Ramos Cozzi", "", "gabriel.ramoscozzi@gmail.com")).build();
	}
}