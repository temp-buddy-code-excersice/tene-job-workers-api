package com.tenejob.tenejobworkersapi.models;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;

import org.junit.Test;

import com.tenejob.tenejobworkersapi.testutils.ControllerEmbededDBRunner;
import com.tenejob.tenejobworkersapi.utils.Day;

public class ShiftTest extends ControllerEmbededDBRunner {

	@Test
	public void createShift() {
		Shift s = new Shift();

		assertNotNull(s);
		assertNotNull(s.getDay());
		assertNotNull(s.getId());
	}

	@Test
	public void setters() {
		Shift s = new Shift();
		HashSet<Day> days = new HashSet<Day>();
		days.add(Day.Monday);
		s.setDay(days);
		s.setId(2);

		assertEquals(s.getId(), Integer.valueOf(2));
		assertTrue(s.getDay().contains(Day.Monday));
	}

	@Test
	public void equalsShifts() {
		Shift s1 = new Shift();
		s1.setId(5);
		s1.getDay().add(Day.Monday);
		Shift s2 = new Shift();
		s2.setId(5);
		s2.getDay().add(Day.Monday);

		assertEquals(s1.getId(), s2.getId());
		assertEquals(s1.getDay(), s2.getDay());
	}

	@Test
	public void nonEquals() {
		Shift s1 = new Shift();
		s1.setId(1);
		s1.getDay().add(Day.Wednesday);

		Shift s2 = new Shift();
		s2.setId(5);
		s2.getDay().add(Day.Monday);

		assertNotEquals(s1.getId(), s2.getId());
		assertNotEquals(s1.getDay(), s2.getDay());
	}

}
