package com.tenejob.tenejobworkersapi.models;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;

import org.junit.Test;

import com.tenejob.tenejobworkersapi.testutils.ControllerEmbededDBRunner;
import com.tenejob.tenejobworkersapi.utils.Day;

public class WorkerTest extends ControllerEmbededDBRunner {

	@Test
	public void createWorker() {
		Worker w = new Worker();

		assertNotNull(w);
		assertNotNull(w.getAvailability());
		assertNotNull(w.getId());
		assertNotNull(w.getPayrate());
	}

	@Test
	public void setters() {
		Worker w = new Worker();
		HashSet<Day> days = new HashSet<Day>();
		days.add(Day.Monday);
		w.setAvailability(days);
		w.setId(2);
		w.setPayrate(9.24);

		assertEquals(w.getId(), Integer.valueOf(2));
		assertEquals(w.getPayrate(), Double.valueOf(9.24));
		assertTrue(w.getAvailability().contains(Day.Monday));
	}

	@Test
	public void equalsWorkers() {
		Worker w1 = new Worker();
		w1.setPayrate(2.98);
		w1.setId(5);
		w1.getAvailability().add(Day.Monday);
		Worker w2 = new Worker();
		w2.setPayrate(2.98);
		w2.setId(5);
		w2.getAvailability().add(Day.Monday);

		assertEquals(w1.getId(), w2.getId());
		assertEquals(w1.getAvailability(), w2.getAvailability());
		assertEquals(w1.getPayrate(), w2.getPayrate());
	}

	@Test
	public void nonEquals() {
		Worker w1 = new Worker();
		w1.setPayrate(2.92);
		w1.setId(1);
		w1.getAvailability().add(Day.Wednesday);

		Worker w2 = new Worker();
		w2.setPayrate(2.98);
		w2.setId(5);
		w2.getAvailability().add(Day.Monday);

		assertNotEquals(w1.getId(), w2.getId());
		assertNotEquals(w1.getAvailability(), w2.getAvailability());
		assertNotEquals(w1.getPayrate(), w2.getPayrate());
	}

}
