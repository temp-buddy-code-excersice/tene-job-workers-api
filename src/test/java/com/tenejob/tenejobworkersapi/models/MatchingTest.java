package com.tenejob.tenejobworkersapi.models;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.tenejob.tenejobworkersapi.testutils.ControllerEmbededDBRunner;

public class MatchingTest extends ControllerEmbededDBRunner {

	@Test
	public void createMatching() {
		Matching s = new Matching();

		assertNotNull(s);
		assertNotNull(s.getWorker());
		assertNotNull(s.getShift());
		assertNotNull(s.getId());
	}

	@Test
	public void setters() {
		Matching m = new Matching();
		Worker w = new Worker();
		w.setId(98);
		Shift s = new Shift();
		s.setId(99);

		m.setId(2);
		m.setWorker(w);
		m.setShift(s);

		assertEquals(Integer.valueOf(2), m.getId());
		assertEquals(Integer.valueOf(98), m.getWorkerId());
		assertEquals(Integer.valueOf(99), m.getShiftId());
	}

	@Test
	public void equalsMatchings() {
		Worker w = new Worker();
		w.setId(213);
		Shift s = new Shift();
		s.setId(132);

		Matching m1 = new Matching();
		m1.setId(5);
		m1.setWorker(w);
		m1.setShift(s);

		Matching m2 = new Matching();
		m2.setId(5);
		m2.setWorker(w);
		m2.setShift(s);

		assertEquals(m1.getId(), m2.getId());
		assertEquals(m1.getWorker(), m2.getWorker());
		assertEquals(m1.getShift(), m2.getShift());
	}

	@Test
	public void nonEquals() {
		Matching m1 = new Matching();
		m1.setId(1);
		m1.setWorker(new Worker());
		m1.setShift(null);

		Matching m2 = new Matching();
		m2.setId(5);
		m2.setWorker(null);
		m2.setShift(new Shift());

		assertNotEquals(m1.getId(), m2.getId());
		assertNotEquals(m1.getWorker(), m2.getWorker());
		assertNotEquals(m1.getShift(), m2.getShift());
	}

	@Test
	public void constructorWithParams() {
		Matching m = new Matching(new Worker(), new Shift());

		assertNotNull(m);
		assertNotNull(m.getWorker());
		assertNotNull(m.getShift());
	}

}
