package com.tenejob.tenejobworkersapi;


import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource("classpath:test.properties")
@SpringBootTest(classes = TeneJobWorkersApiApplication.class)
public class TeneJobWorkersApiApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Test
	public void main() {
		TeneJobWorkersApiApplication app = new TeneJobWorkersApiApplication();
		TeneJobWorkersApiApplication.main(new String[0]);
		assertNotNull(app);
	}
}
