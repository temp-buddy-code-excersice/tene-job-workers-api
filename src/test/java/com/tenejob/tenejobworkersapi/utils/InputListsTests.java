package com.tenejob.tenejobworkersapi.utils;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.tenejob.tenejobworkersapi.models.Shift;
import com.tenejob.tenejobworkersapi.models.Worker;

public class InputListsTests {

	@Test
	public void constructor() {
		InputLists iL = new InputLists();

		assertNotNull(iL);
		assertNotNull(iL.getShifts());
		assertNotNull(iL.getWorkers());
	}

	@Test
	public void setters() {
		InputLists iL = new InputLists();

		List<Worker> workers = new ArrayList<>();
		workers.add(new Worker());
		List<Shift> shifts = new ArrayList<>();
		shifts.add(new Shift());

		iL.setWorkers(workers);
		iL.setShifts(shifts);

		assertEquals(1, iL.getWorkers().size());
		assertEquals(1, iL.getShifts().size());
	}
}
