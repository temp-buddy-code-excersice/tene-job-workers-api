package com.tenejob.tenejobworkersapi.utils;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.tenejob.tenejobworkersapi.models.Matching;

public class OutputMappingsLTest {

	@Test
	public void constructor() {
		OutputMappings oM = new OutputMappings();

		assertNotNull(oM);
		assertNotNull(oM.getMappings());
		assertEquals(Double.valueOf(0), oM.getTotalCost());
	}

	@Test
	public void setters() {
		List<Matching> matchings = new ArrayList<>();
		matchings.add(new Matching());

		OutputMappings oM = new OutputMappings();
		oM.setMappings(matchings);
		oM.setTotalCost(1.23);
		oM.addTotalCost(2.00);

		assertEquals(1, oM.getMappings().size());
		assertEquals(Double.valueOf(3.23), oM.getTotalCost());
	}
}
