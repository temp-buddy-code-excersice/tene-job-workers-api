package com.tenejob.tenejobworkersapi.utils;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.tenejob.tenejobworkersapi.models.Shift;
import com.tenejob.tenejobworkersapi.models.Worker;

public class MatchingPairTest {

	@Test
	public void createMatching() {
		MatchingPair mP = new MatchingPair();

		assertNotNull(mP);
		assertNotNull(mP.getShift());
		assertNotNull(mP.getWorkers());
	}

	@Test
	public void setters() {
		MatchingPair mP = new MatchingPair();
		List<Worker> workers = new ArrayList<Worker>();

		Worker w = new Worker();
		workers.add(w);

		Shift s = new Shift();
		s.setId(1);

		mP.setShift(s);
		mP.setWorkers(workers);

		assertEquals(Integer.valueOf(1), mP.getShift().getId());
		assertEquals(1, mP.getWorkers().size());
	}

	@Test
	public void equalsMatchings() {
		MatchingPair mP1 = new MatchingPair();
		MatchingPair mP2 = new MatchingPair();

		List<Worker> workers = new ArrayList<Worker>();

		Worker w = new Worker();
		w.setId(1);
		workers.add(w);

		Shift s = new Shift();
		s.setId(1);

		mP1.setShift(s);
		mP2.setShift(s);

		mP1.setWorkers(workers);
		mP2.setWorkers(workers);

		assertEquals(mP1.getShift().getId(), mP2.getShift().getId());
		assertEquals(mP1.getWorkers().size(), mP2.getWorkers().size());
		assertEquals(mP1.getWorkers().get(0).getId(), mP2.getWorkers().get(0).getId());
	}

	@Test
	public void nonEquals() {
		MatchingPair mP1 = new MatchingPair();
		MatchingPair mP2 = new MatchingPair();

		List<Worker> workers = new ArrayList<Worker>();

		Worker w = new Worker();
		w.setId(1);
		workers.add(w);
		mP1.setWorkers(workers);

		Worker w2 = new Worker();
		w.setId(2);

		Shift s1 = new Shift();
		s1.setId(1);

		Shift s2 = new Shift();
		s2.setId(2);

		mP1.setShift(s1);
		mP2.setShift(s2);

		workers.add(w2);

		mP2.getWorkers().add(w2);

		assertNotEquals(mP1.getShift().getId(), mP2.getShift().getId());
		assertNotEquals(mP1.getWorkers().size(), mP2.getWorkers().size());
		assertNotEquals(mP1.getWorkers().get(0).getId(), mP2.getWorkers().get(0).getId());
	}

	@Test
	public void constructorWithParams() {
		MatchingPair mP = new MatchingPair(new Shift(), new ArrayList<>());

		assertNotNull(mP);
		assertNotNull(mP.getShift());
		assertNotNull(mP.getWorkers());
	}
}
