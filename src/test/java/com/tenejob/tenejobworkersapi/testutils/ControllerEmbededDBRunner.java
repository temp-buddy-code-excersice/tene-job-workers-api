package com.tenejob.tenejobworkersapi.testutils;


import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.tenejob.tenejobworkersapi.TeneJobWorkersApiApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { TeneJobWorkersApiApplication.class })
@WebAppConfiguration
@DataJpaTest
@TestPropertySource("classpath:test.properties")
public abstract class ControllerEmbededDBRunner {

	@Autowired
	private WebApplicationContext webApplicationContext;

	public MockMvc mockMvc;

	@Before
	public void setUp() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
	}

	protected <R> R doGet(String url, Class<R> resultClass) {
		try {
			MvcResult resultOfRequest = mockMvc
			        .perform(MockMvcRequestBuilders.get(url).contentType(MediaType.APPLICATION_JSON)).andReturn();
			return ControllerTestUtils.fromJsonStringToObject(resultOfRequest.getResponse().getContentAsString(),
			        resultClass);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String doGet(String url) {
		try {
			MvcResult resultOfRequest = mockMvc
			        .perform(MockMvcRequestBuilders.get(url).contentType(MediaType.APPLICATION_JSON)).andReturn();
			return resultOfRequest.getResponse().getContentAsString();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected <R> R doGetWithParams(Class<R> resultClass, RequestBuilder builder) {
		try {
			MvcResult resultOfRequest = mockMvc.perform(builder).andReturn();
			return ControllerTestUtils.fromJsonStringToObject(resultOfRequest.getResponse().getContentAsString(),
			        resultClass);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected <E, T> T doPostWithJson(String url, E objectToSend, Class<T> objectToReceiveClass) {
		try {
			MvcResult resultOfRequest = mockMvc.perform(MockMvcRequestBuilders.post(url)
			        .contentType(MediaType.APPLICATION_JSON).content(ControllerTestUtils.asJsonString(objectToSend)))
			        .andReturn();
			return ControllerTestUtils.fromJsonStringToObject(resultOfRequest.getResponse().getContentAsString(),
			        objectToReceiveClass);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected <E> String doPostWithJsonReturnsPlainText(String url, String key, String value) {
		try {
			MvcResult resultOfRequest = mockMvc
			        .perform(MockMvcRequestBuilders.post(url).contentType(MediaType.APPLICATION_JSON).param(key, value))
			        .andReturn();
			return resultOfRequest.getResponse().getContentAsString();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected MvcResult doDeleteById(String url, Long id) {
		try {
			MvcResult resultOfRequest = mockMvc.perform(MockMvcRequestBuilders.delete(url + "/" + id))
			        .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			return resultOfRequest;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
