package com.tenejob.tenejobworkersapi.service;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;

import com.tenejob.tenejobworkersapi.models.Worker;
import com.tenejob.tenejobworkersapi.testutils.ControllerEmbededDBRunner;
import com.tenejob.tenejobworkersapi.utils.Day;

@Import({ WorkerService.class })
public class WorkerServiceTest extends ControllerEmbededDBRunner {

	@Autowired
	private WorkerService workerService;

	@Test
	public void saveWorker() {
		Worker w = new Worker();
		w.getAvailability().add(Day.valueOf("Monday"));
		w.getAvailability().add(Day.Tuesday);
		Set<Day> days = new HashSet<>();
		days.add(Day.valueOf("Friday"));
		days.add(Day.Wednesday);
		w.setAvailability(days);
		w.setPayrate(2.98);

		Worker newWorker = this.workerService.save(w);

		assertTrue(newWorker.getAvailability().contains(Day.Friday));
		assertTrue(newWorker.getAvailability().contains(Day.Wednesday));
		assertEquals(w.getPayrate(), newWorker.getPayrate());
	}

	@Test
	public void getAllWorkersEmpty() {
		assertTrue(this.workerService.findAll().isEmpty());
	}

	@Test
	public void getsAll10Workers() {
		for (int i = 0; i < 10; i++) {
			this.workerService.save(new Worker());
		}

		assertTrue(!this.workerService.findAll().isEmpty());
		assertEquals(10, this.workerService.findAll().size());
	}

	@Test
	public void deleteWorker() {
		Worker w = new Worker();
		w.setId(0);
		w.setPayrate(2.98);
		Worker newWorker = this.workerService.save(w);

		assertTrue(!this.workerService.findAll().isEmpty());

		this.workerService.delete(newWorker.getId());

		assertTrue(this.workerService.findAll().isEmpty());
	}

	@Test
	public void findOneWorker() {
		Worker w = new Worker();
		w.setId(0);
		w.setPayrate(2.98);

		Worker newWorker = this.workerService.save(w);

		assertEquals(newWorker, this.workerService.findOne(newWorker.getId()));
		assertTrue(newWorker.getPayrate() == 2.98);
	}

}
