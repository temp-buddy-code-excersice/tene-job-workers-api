package com.tenejob.tenejobworkersapi.service;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;

import com.tenejob.tenejobworkersapi.models.Shift;
import com.tenejob.tenejobworkersapi.testutils.ControllerEmbededDBRunner;
import com.tenejob.tenejobworkersapi.utils.Day;

@Import({ ShiftService.class })
public class ShiftServiceTest extends ControllerEmbededDBRunner {

	@Autowired
	private ShiftService shiftService;

	@Test
	public void saveShift() {
		Shift s = new Shift();
		s.getDay().add(Day.Monday);
		s.getDay().add(Day.Friday);

		Set<Day> days = new HashSet<>();
		days.add(Day.Monday);
		days.add(Day.Friday);
		s.setDay(days);
		Shift newShift = this.shiftService.save(s);

		assertEquals(s.getDay(), newShift.getDay());
		assertTrue(newShift.getDay().contains(Day.Monday));
		assertTrue(newShift.getDay().contains(Day.Friday));
	}

	@Test
	public void getAllShiftsEmpty() {
		assertTrue(this.shiftService.findAll().isEmpty());
	}

	@Test
	public void getAll10Shifts() {
		for (int i = 0; i < 10; i++) {
			this.shiftService.save(new Shift());
		}

		assertTrue(!this.shiftService.findAll().isEmpty());
		assertEquals(10, this.shiftService.findAll().size());
	}

	@Test
	public void findOneShift() {
		Shift s = new Shift();
		s.setId(0);
		s.getDay().add(Day.Monday);

		Shift newShift = this.shiftService.save(s);

		assertEquals(newShift, this.shiftService.findOne(newShift.getId()));
		assertTrue(newShift.getDay().contains(Day.Monday));
	}

	@Test
	public void deleteShift() {
		Shift s = new Shift();
		s.setId(0);
		Shift newShift = this.shiftService.save(s);

		assertTrue(!this.shiftService.findAll().isEmpty());

		this.shiftService.delete(newShift.getId());

		assertTrue(this.shiftService.findAll().isEmpty());
	}

}
